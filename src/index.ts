
import express from 'express';
import path from 'path';
import { engine } from 'express-handlebars';
import IndexRoutes from  './routes';
import BooksRoutes from './routes/books'
const initDB = require('./database')

//Initializations
const app = express();

app.set('port',3000);
app.set('views', 'hbs')
//Middlewares
app.use(express.json()); 
app.use(express.urlencoded({extended:false})) //Poder interpretar datos enviado por formularios

//handlebars
app.engine('.hbs', engine({
    extname: '.hbs',
    partialsDir: './src/views/partials',
    layoutsDir: './src/views/layouts',
    defaultLayout: 'index'
}));
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname,'./views/renders'));


//Routes
app.use('/', IndexRoutes)
app.use('/books', BooksRoutes)

//Static files
app.use(express.static(path.join(__dirname, 'public') ));


//starting de server
app.listen(app.get('port'),()=>{
    console.log(`Server port ${app.get('port')}`)
});
//start DB
initDB()