import { Request,Response} from 'express';
import  BookModel, { BookInterface } from '../models/booksModel';

class BooksController {
    public listBooks = async (req: Request, res:Response)=>{
        const books:BookInterface[] = await BookModel.find().lean()
        console.log(books)
        res.render('indexBooks',{
            title: 'Books',
            books
        })
    }

    public renderFormBook(req: Request, res: Response):void {
        res.render('addBooks',{
            title: 'AddBooks'
        });    
    }
    public saveBook = async(req: Request, res: Response)=>{
        const { title, author, isbn} = req.body;
        const book: BookInterface = new BookModel({title, author, isbn})
        await book.save();
        res.redirect('/Books');
    }
}

export const booksController = new BooksController();