import { Router,Request, Response } from "express";

const router : Router = Router();

import { booksController } from "../controllers/booksCotroller";

router.get('/',booksController.listBooks);
router.post('/add/aa',booksController.saveBook);
router.get('/add',booksController.renderFormBook);



export default router;