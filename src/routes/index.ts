import  { Router } from 'express';

const router = Router();

import { indexControler } from '../controllers/indexControlers';

router.get('/',indexControler.index);

export default router;