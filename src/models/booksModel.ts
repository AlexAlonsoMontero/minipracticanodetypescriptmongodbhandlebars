import mongoose, { model, Schema } from 'mongoose';

export interface BookInterface extends mongoose.Document {
    title:string;
    author:string;
    isbn: string;
}

const BookSchema  = new Schema({
    title: String,
    author: String,
    isbn: String
})

export default model<BookInterface>('Book',BookSchema) //indico que el modelo va a ser del tipo interface Book