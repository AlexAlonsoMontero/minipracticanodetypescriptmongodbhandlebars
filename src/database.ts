import mongoose from 'mongoose';

import { mongodb } from './keys'

module.exports = () =>{
    console.log("entra")
    const connect =async () => {
        try{
            await mongoose.connect( mongodb.URI);
            console.log("Conexión ok")
            }catch(e){
                console.log("No se ha podido conectar a base de datos")
            }    
    }
    connect();
}