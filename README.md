# Api sencilla usando MongoDB, NodeJS y TypeScritpt

## Instalamos express y handlebars
- [ ] https://www.npmjs.com/package/express
- [ ] https://www.npmjs.com/package/handlebars

```
npm i express express-handlebars
npm i @types/express-handlebars
```
## Instalamos mongose 
* Conetor base de datos MongoDB
```
npm i express mongose
```

## Instalamos TypeScript
```
npm install -g typecript
tsc --init
```

## Configuramos TypeScript tsconfig.json
* Descomentamos la línea    "outDir":  y añadimos "./build/src", esto hace que los ficheros js que crea TypesCrip se generen en esa carpeta.
* Descomentamos la línea  // "moduleResolution": "node", esto indica que es TypeSctipt para node
* Agregamos al final del archivo el siguiente código, indicamos a Typescript en que carpetas revisar para compilar y en cuales no.
```
},
"include":[
    "./src/**/*"
  ],
  "exclude":[
    "node_modules"
  ]
```

## Instalamos como dependencia de desarrollo @types/express
```
npm i @types/express -D
```
## instalamos TypeScript en el proyecto
```
npm i typescript
```

## Configuramos packaje.json, carpeta script
* clean -- limpiamos carpeta build para eliminar build
* tsc -- interpritar el typescript
* start -- arrancar el sever leyendo el index.js
* ts:node -- Ejecutar Typescript en node
    * instalamos ts:node 
```
 "scripts": {
    "ts:node": "ts-node src/index.ts",
    "test": "echo \"Error: no test specified\" && exit 1",
    "clean": "rm -rf build",
    "build": "tsc",
    "start": "node  build/src",
    "dev": "nodemon --config nodemon.json src/index.ts"
  }
```

## Instalamos nodemon y lo configuramos para typescript
```
npm i nodemon
```

* Indicamos a nodemon cuando reiniciar, que comando ejectuar que carpetas revisar y cuales ignorar el archivo nodemon.js
```
{
    "restartable": "rs",
    "ignore": [".git", "node_modules/", "dist/", "coverage/"],
    "watch": ["src/"],
    "execMap": {
      "ts": "node -r ts-node/register"
    },
    "ext": "js,json,ts"
}
```

## BASE DE DATOS
* Instalamos types de mongoose
